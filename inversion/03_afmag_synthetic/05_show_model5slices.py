# -*- coding: utf-8 -*-
"""
Created on Tue Nov  7 12:21:17 2017

@author: Rochlitz.R
"""

import matplotlib as mpl
#mpl.use('pdf')
from matplotlib import cm, rcParams

import numpy as np
import pyvista as pv
import matplotlib.pyplot as plt
from scipy.spatial import ConvexHull
from matplotlib.colors import LogNorm
from custEM.meshgen import meshgen_utils as mu



# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # # plot resistivity model along slices # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


# specify to plot either p1 or p2 results
p = 2

# %% define plot parameters and import results

# make nice colorbar
class MplColorHelper:

  def __init__(self, cmap_name, start, stop):
    self.cmap_name = cmap_name
    self.cmap = plt.get_cmap(cmap_name)
    self.norm = mpl.colors.LogNorm(vmin=start, vmax=stop)
    self.scalarMap = cm.ScalarMappable(norm=self.norm, cmap=self.cmap)

  def get_rgb(self, val):
    return self.scalarMap.to_rgba(val)

# specify color bar parameters
cmap = 'Spectral'
COL = MplColorHelper(cmap, 5., 5000.)

# load final resistivity model
path = './inv_results/p' + str(p) + 'afmag_schleiz_inv_p' + str(p) + '/'
mesh = pv.read(path+"latest_invmodel.vtk")
anoms = pv.read("data/anoms.vtk")
anomres = 1./np.array([5e-4, 2e-2, 2e-4, 5e-2, 1e-1])

# rotate to x' / y' coordinate frame, so y' = y is parallel to flight lines
center = [np.mean([np.min(mesh.points[:, 0]), np.max(mesh.points[:, 0])]),
          np.mean([np.min(mesh.points[:, 1]), np.max(mesh.points[:, 1])]),
          0.]
mesh.points = mu.rotate(mesh.points, np.deg2rad(37))
anoms.points = mu.rotate(anoms.points, np.deg2rad(37))

# %% plot results

# specify values to plot slices along rotated plot axis (x also possible)
normal = 'y'
scalars = 'res'
offsets = np.linspace(-1.2e3, 1.2e3, 5)

# create figure
fig, ax = plt.subplots(5, 1, figsize=[8, 12], sharex=True, sharey=True)
for j, yoff in enumerate(offsets):
    # slice through model
    data = mesh.slice(normal=normal, origin=[0., yoff, 0.],
                      generate_triangles=True)

    data2 = anoms.slice(normal=normal, origin=[0., yoff, 0.],
                        generate_triangles=True)

    # get intersections of anomalies on slice to plot on resistivity model
    outl = []
    points = []
    for thres in range(2, 7):
        data3 = data2.threshold([thres, thres + 0.1])
        points.append(data3.points[:, ::2])

        if len(points[-1]) > 0:
            outl.append(ConvexHull(points[-1]))
        else:
            outl.append([])

    # get triangulation
    x = data.points
    tri = data.faces.reshape((-1, 4))[:, 1:]
    u = data[scalars]

    if normal == 'x':
        a, b = x[:, 1], x[:, 2] # yz
    elif normal == 'y':
        a, b = x[:, 0], x[:, 2] # xz
    elif normal == 'z':
        a, b = x[:, 0], x[:, 1] # xy

    # plot resistivities
    fig = ax[j].tripcolor(a, b, tri, u, rasterized=True, cmap=cmap,
                          norm=LogNorm(vmin=5., vmax=5000.))

    # plot anomaly frames
    for ei, elem in enumerate(outl):
        if type(elem) is not list:
            for simplex in elem.simplices:
                ax[j].plot(points[ei][simplex, 0], points[ei][simplex, 1],
                           c=COL.get_rgb(anomres[ei]))

    # adjust axes and labels
    ax[j].set_aspect('equal')
    ax[j].set_xlim([np.min(data.points[:, 0]),
                    np.max(data.points[:, 0])])
    ax[j].set_ylim([np.min([-1500., 500.]),
                    np.max(data.points[:, 2])])
    ax[j].set_yticks([-1200., -800., -400., 0., 400.])
    ax[j].set_yticklabels(['-1200', '', '-400' , '', '400'], fontsize=14)
    if j == 11:
        ax[j].set_xlabel("x' (m)", fontsize=14)
    ax[j].set_ylabel("z (m)", fontsize=14)
    ax[j].text(-2400., -1200., "y' = " + str(yoff) + ' m', fontsize=14,
               c='#002C72')
    ax[j].set_xlim([-3000., 5000.])

# add title and colorbar
plt.suptitle('Recovered resistivity model: p' + str(p))
cbar = plt.colorbar(fig, ax=ax)
cbar.set_label(r'Resistivity ($\Omega$m)', rotation=90)
cbar.ax.tick_params(labelsize=14)

# save figure
# plt.savefig('AFMAG_synth_invmodel_p' + str(p) + '.pdf', bbox_inches='tight')
plt.savefig('AFMAG_synth_invmodel_p' + str(p) + '.png', bbox_inches='tight')
