# -*- coding: utf-8 -*-
"""
Created on Tue Nov  7 12:21:17 2017

@author: Rochlitz.R
"""


import matplotlib
#matplotlib.use('pdf')
from saem import CSEMSurvey
from matplotlib import cm, rcParams
import matplotlib.pyplot as plt


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # # plot data and final response misfit # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


# %% import results and use plot utilities from SAEM class

# specify p
p = 1

# import results
data = CSEMSurvey('data/Txy.npz', mtdata=True)

# data = CSEMSurvey('data/ExyBxyzToPlot.npz')
# data.patches[0].cmp = [1, 1, 0]  # hack cmp detection for E-field data,
# data.patches[1].cmp = [1, 1, 0]  # hack cmp detection for E-field data,
# data.loadResults(dirname='inv_results/p' + str(p) + '_schleiz_inv_p' + str(p))

# patches[0] ... E-field Rx of Tx 0
# patches[1] ... E-field Rx of Tx 1
# patches[2] ... B-field Rx of Tx 0 (without Rx in vicinity of Tx)
# patches[3] ... B-field Rx of Tx 1 (without Rx in vicinity of Tx)
# patches[4] ... B-field Rx of Tx 0 in vicinity of Tx 1
# patches[5] ... B-field Rx of Tx 1 in vicinity of Tx 0

# %% generate pdf files

for pi, patch in enumerate(data.patches):
    if pi < 2:  # E-field surface Rx stations
        patch.generateDataPDF(pdffile='p' + str(p) + '_misfit_patch_' +
                              str(pi) + '.pdf', mode='linefreqwise',
                              alim=[1e-7, 1e-3], field='E')
    else:  # B-field Rx in the air
        patch.generateDataPDF(pdffile='p' + str(p) + '_misfit_patch_' +
                              str(pi) + '.pdf', mode='linefreqwise')