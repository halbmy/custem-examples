# -*- coding: utf-8 -*-
"""
Created on Tue Nov  7 12:21:17 2017

@author: Rochlitz.R
"""


import numpy as np
import pygimli as pg
from custEM.meshgen import meshgen_utils as mu
from custEM.meshgen.meshgen_tools import BlankWorld
from custEM.inv.inv_utils import MultiFWD
from saem import CSEMSurvey


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # # # #  run synthetic data inversion # # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


# %% specify polnomial order, suited refinement parameters are chosen below
# accordingly
p = 1
lam = 10.
invmod = 'p' + str(p)
invmesh = 'schleiz_inv_p' + str(p)

# %% create mesh

# define ground Rx
xr = np.linspace(-1000., 1000., 11)
yr = np.linspace(-1000., 1000., 11)
rx = np.zeros((121, 3))

a = 0
for x in xr:
    for y in yr:
        rx[a, 0] = x
        rx[a, 1] = y
        a += 1

rx_tri = mu.refine_rx(rx, 10., 30.)

inv_area = np.array([[-1100., -1100., 0.],
                     [1100., -1100., 0.],
                     [1100., 1100., 0.],
                     [-1100., 1100., 0.]])

dim = 1e4
M = BlankWorld(x_dim=[-dim, dim],
               y_dim=[-dim, dim],
               z_dim=[-dim, dim],
               name=invmesh,
               preserve_edges=True,
               )

M.build_surface(insert_line_tx=[mu.line_y(-1.1e3, 1.1e3, n_segs=51, x=-1.1e3),
                                mu.line_y(-1.1e3, 1.1e3, n_segs=51, x=1.1e3)],
                insert_paths=rx_tri)

M.add_inv_domains(-1000., inv_area, cell_size=1e7,
                  x_frame=1e3, y_frame=1e3, z_frame=1e3,)

M.build_halfspace_mesh()

rx[:, 2] = -1.
M.add_rx(rx)
M.call_tetgen(tet_param='-pq1.6aA')

# %% define inversion parameter

sig_bg = 2e-3
max_iter = 21
synth_data = np.load('data/BxyzToImport.npz', allow_pickle=True)
toplot = CSEMSurvey('data/BxyzToPlot.npz')

# %% set up forward operator
fop = MultiFWD(invmod, invmesh, saem_data=synth_data,
               sig_bg=sig_bg, p_fwd=p, max_procs=6)
fop.setRegionProperties("*", limits=[1e-4, 1e0])

# %% set up inversion operator
inv = pg.Inversion(verbose=True)  # , debug=True)
inv.setForwardOperator(fop)
inv.setPostStep(fop.analyze)
dT = pg.trans.TransSymLog(1e-3)
inv.dataTrans = dT

# %% run inversion
invmodel = inv.run(fop.measured, fop.errors, lam=lam, verbose=True,
                   startModel=fop.sig_0, maxIter=max_iter)

# %% save results
np.save(fop.inv_dir + 'final_inv_model.npy', invmodel)
# invmodel = np.load(fop.inv_dir + 'inv_model.npy')
res = 1. / invmodel
inner = fop.mesh()
inner['sigma'] = invmodel
inner['res'] = res
cov = np.zeros(fop._jac.cols())
for i in range(fop._jac.rows()):
    cov += np.abs(fop._jac.row(i))
cov /= inner.cellSizes()
cov /= res
np.save(fop.inv_dir + invmod + '_coverage.npy', cov)
inner['coverage'] = cov
inner.exportVTK(fop.inv_dir + invmod + '_final_invmodel.vtk')