# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""


import numpy as np
from custEM.meshgen import meshgen_utils as mu
from custEM.meshgen.meshgen_tools import BlankWorld
from custEM.inv.inv_utils import MultiFWD


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # #  create synthetic data for inversion  # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# fix random seeed for reproducibility
np.random.seed(99999)
mod = 'synth_data'
mesh = 'synth_plate'

# %% mesh and survey parameter definitions

# define airborne Rx
rx_air = mu.rx_grid([-3000., 3000., 61], [-1200., 1200., 25], z=50.)
txs = [mu.line_y(-0.95e3, 0.55e3, n_segs=21, x=-1.05e3),
       mu.line_y(-0.55e3, 0.95e3, n_segs=21, x=0.85e3)]

dim = 1e4
M = BlankWorld(x_dim=[-dim, dim],
               y_dim=[-dim, dim],
               z_dim=[-dim, dim],
               name=mesh,
               preserve_edges=True,
               )

M.build_surface(insert_line_tx=txs)
M.build_halfspace_mesh()

M.add_plate(dx=1000., dy=1000., dz=200., origin=[-200., 200., -600.0],
            dip=70., dip_azimuth=33.,
            cell_size=1e7)

rx_tri = mu.refine_rx(rx_air, 10., 30.)
M.add_paths(rx_tri)
M.add_rx(rx_air)

M.extend_world(10., 10., 10.)
M.call_tetgen(tet_param='-pq1.6aA')

# %% set synthetic data generation parameters

err = 0.05        #                           mu_0         nT
noise_B = 1e-3    # related to expected tipper data amplitude ranges
p_fwd = 2         # polynomial order for forward modeling
n_cores=72        # total number of cores used
min_freqs = None  # set to 2, 3 or 4 to trade time vs. RAM requirements,
#                 # e.g., 3 means each thread calculates 3 freqs,
#                 # so time is tripled but RAM requirements are only 1/3

freqs = [6., 10., 20., 35., 60., 100., 200., 350., 600., 1000., 2000., 3500.]
cmps = [['Bx', 'By', 'Bz']]
tx_ids = [[0, 1]]

# for anomaly marker
sig_m = [1e-1]
# for marker 1, surrounding halfspace
sig_bg = 1e-3


# %% create synthetic data without IP effects

pfname = 'Bxyz'
fop = MultiFWD(mod, mesh, list(freqs), cmps, tx_ids, sig_bg=sig_bg,
               p_fwd=p_fwd, min_freqs=min_freqs, n_cores=n_cores)
data = fop.response(sig_m)
np.save('data/' + pfname + '_no_noise.npy', data)

data = np.load('data/' + pfname + '_no_noise.npy')
# define abs_error for all recordoings (B fields)
abs_error = np.abs(data) * err + noise_B

# add noise
data += np.random.randn(len(data)) * abs_error

# remove very weak amplitudes for E_real, B_real, E_imag, B_imag
data[np.abs(data) < noise_B*0.5] = np.nan

# convert column vector to data structure required by inversion module
fop.export_npz('data/' + pfname, data, abs_error)

# %% create synthetic data with IP effects of anomaly

pfname = 'BxyzIP'
fop = MultiFWD(mod, mesh, list(freqs), cmps, tx_ids, sig_bg=sig_bg,
               p_fwd=p_fwd, min_freqs=min_freqs, n_cores=n_cores)
fop.config['ip_c'] = [0., 0., 0.4]
fop.config['ip_m'] = [0., 0., 0.8]
fop.config['ip_tau'] = [0., 0., 0.6]
data = fop.response(sig_m)
np.save('data/' + pfname + '_no_noise.npy', data)

data = np.load('data/' + pfname + '_no_noise.npy')
# define abs_error for all recordoings (B fields)
abs_error = np.abs(data) * err + noise_B

# add noise
data += np.random.randn(len(data)) * abs_error

# remove very weak amplitudes for E_real, B_real, E_imag, B_imag
data[np.abs(data) < noise_B*0.5] = np.nan

# convert column vector to data structure required by inversion module
fop.export_npz('data/' + pfname, data, abs_error)