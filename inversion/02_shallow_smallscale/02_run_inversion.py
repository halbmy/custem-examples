# -*- coding: utf-8 -*-
"""
Created on Tue Nov  7 12:21:17 2017

@author: Rochlitz.R
"""


import numpy as np
import pygimli as pg
from custEM.meshgen import meshgen_utils as mu
from custEM.meshgen.meshgen_tools import BlankWorld
from custEM.inv.inv_utils import MultiFWD
from saem import CSEMSurvey


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # # # #  run synthetic data inversion # # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


# %% specify polnomial order, suited refinement parameters are chosen below
# accordingly

h = 10.
p = 1
lam = 5.
lamfac = 0.8
invmod = 'p' + str(p) + '_' + str(h) + '_16khz'
invmesh = 'shallow_h10_p' + str(p)

# %% create mesh

# define drone Rx
xr = np.linspace(0., 500., 21)
yr = np.linspace(0., 500., 11)
rx = np.zeros((21*11, 3))


a = 0
for x in xr:
    for y in yr:
        rx[a, 0] = x
        rx[a, 1] = y
        a += 1

inv_area = np.array([[0., 0., 0.],
                     [500., 0., 0.],
                     [500., 500., 0.],
                     [0., 500., 0.]])

dim = 1e4
M = BlankWorld(x_dim=[-dim, dim],
               y_dim=[-dim, dim],
               z_dim=[-dim, dim],
               name=invmesh,
               preserve_edges=True,
               )

M.build_surface(insert_line_tx=[mu.line_y(-200., 800., n_segs=51, x=-200.),
                                mu.line_x(-200., 800., n_segs=51, y=-200.)])

M.add_inv_domains(-100., inv_area, cell_size=2e2,
                  x_frame=5e2, y_frame=5e2, z_frame=5e2,)

M.build_halfspace_mesh()

rx[:, 2] = h
rx_tri = mu.refine_rx(rx, 2., 30.)
M.add_rx(rx)
M.add_paths(rx_tri)
M.call_tetgen(tet_param='-pq1.2aA')

# %% define inversion parameter

sig_bg = 1e-2
max_iter = 21
synth_data = np.load('data/Bxyz_h' + str(h) + '_16khzToImport.npz',
                     allow_pickle=True)

# %% set up forward operator
fop = MultiFWD(invmod, invmesh, saem_data=synth_data,
               sig_bg=sig_bg, p_fwd=p, max_procs=4)
fop.setRegionProperties("*", limits=[1e-4, 1e0])

# %% set up inversion operator
inv = pg.Inversion(verbose=True)  # , debug=True)
inv.setForwardOperator(fop)
inv.setPostStep(fop.analyze)
# dT = pg.trans.TransSymLog(1e-3)
# inv.dataTrans = dT

# %% run inversion
invmodel = inv.run(fop.measured, fop.errors, lam=lam,  lamFactor=lamfac,
                   verbose=True, startModel=fop.sig_0, maxIter=max_iter)

# %% save results
np.save(fop.inv_dir + 'final_inv_model.npy', invmodel)
# invmodel = np.load(fop.inv_dir + 'inv_model.npy')
res = 1. / invmodel
inner = fop.mesh()
inner['sigma'] = invmodel
inner['res'] = res

# calculate cummulative sensitvities
cov = np.zeros(fop._jac.cols())
for i in range(fop._jac.rows()):
    cov += np.abs(fop._jac.row(i))
cov /= inner.cellSizes()
cov /= res
np.save(fop.inv_dir + invmod + '_coverage.npy', cov)
inner['coverage'] = cov
inner.exportVTK(fop.inv_dir + invmod + '_final_invmodel.vtk')

# save jacobian for analysis
fop._jac.save(fop.inv_dir + 'jacobian')
