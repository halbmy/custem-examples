# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""


import numpy as np
import matplotlib.pyplot as plt
import pygimli as pg
from custEM.meshgen import meshgen_utils as mu
from custEM.meshgen.meshgen_tools import BlankWorld
from custEM.inv.inv_utils import MultiFWD
import numpy as np
import os
from saem import CSEMSurvey
import zipfile


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # #  create synthetic data for inversion  # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# fix random seeed for reproducibility
np.random.seed(99999)
mod = 'synth_data'
mesh = 'shallow_dense_h10'

# %% mesh and survey parameter definitions

# define ground Rx
xr = np.linspace(0., 500., 51)
yr = np.linspace(0., 500., 26)
rx = np.zeros((51*26, 3))
h = 10.

a = 0
for x in xr:
    for y in yr:
        rx[a, 0] = x
        rx[a, 1] = y
        a += 1

dim = 1e4
M = BlankWorld(x_dim=[-dim, dim],
               y_dim=[-dim, dim],
               z_dim=[-dim, dim],
               name=mesh,
               preserve_edges=True,
               )

M.build_surface(insert_line_tx=[mu.line_y(-200., 800., n_segs=51, x=-200.),
                                mu.line_x(-200., 800., n_segs=51, y=-200.)])

# define anomaly outcrop
anom1 = np.array([[100., 50., 0.],
                  [150., 20., 0.],
                  [180., 400., 0.],
                  [120., 380., 0.]])

M.add_surface_anomaly(insert_paths=[anom1],
                      depths=[-50.],
                      cell_sizes=[1e4],
                      dips=[30.],
                      dip_azimuths=[-70.])

M.build_halfspace_mesh()

M.add_plate(dx=200., dy=300., dz=10., origin=[300., 250., -15.0],
            dip=0., dip_azimuth=0., cell_size=1e4)
M.add_plate(dx=200., dy=300., dz=10., origin=[300., 250., -35.0],
            dip=0., dip_azimuth=0., cell_size=1e4)

rx[:, 2] = h
rx_tri = mu.refine_rx(rx, 2., 30.)
M.add_rx(rx)
M.add_paths(rx_tri)
#M.overwrite_markers=[2, 3]
M.call_tetgen(tet_param='-pq1.4aA')


# %% set synthetic data generation parameters

err = 0.05        #                           mu_0         nT
noise_B = 1e-3    # related to expected tipper data amplitude ranges
p_fwd = 2         # polynomial order for forward modeling
n_cores=72        # total number of cores used
min_freqs = None  # set to 2, 3 or 4 to trade time vs. RAM requirements,
#                 # e.g., 3 means each thread calculates 3 freqs,
#                 # so time is tripled but RAM requirements are only 1/3

#freqs = [32., 96., 200., 350., 600., 1000.]
freqs = [32., 96., 200., 350., 600., 1000.,
         2000., 4000., 6000., 8000., 12000., 16000.]
cmps = [['Bx', 'By', 'Bz']]
tx_ids = [[0, 1]]

# for anomaly marker
sig_m = [0.2, 0.1, 0.02]
# for marker 1, surrounding halfspace
sig_bg = 1e-2


# %% create synthetic data

pfname = 'Bxyz' + str(h) + '_dense'
fop = MultiFWD(mod, mesh, list(freqs), cmps, tx_ids, sig_bg=sig_bg,
               p_fwd=p_fwd, min_freqs=min_freqs, n_cores=n_cores)
data = fop.response(sig_m)
np.save('data/' + pfname + '_no_noise.npy', data)

data = np.load('data/' + pfname + '_no_noise.npy')
# define abs_error for all recordings (B fields)
abs_error = np.abs(data) * err + noise_B

# add noise
data += np.random.randn(len(data)) * abs_error

# remove very weak amplitudes
data[np.abs(data) < noise_B*0.5] = np.nan

# convert column vector to data structure required by inversion module
fop.export_npz('data/' + pfname, data, abs_error)