# -*- coding: utf-8 -*-
"""
@author:  Rochlitz.R
"""

# ########################################################################### #
# # # # #                          example 4                          # # # # #
# ########################################################################### #
# # # # #                      computation script                     # # # # #
# ########################################################################### #

from custEM.core import MOD

M = MOD('p2', 'example_4_mesh', 'E_t', p=2,
        overwrite=True, m_dir='./meshes', r_dir='./results')

# define frequency (11.4 Hz) and condcutivities
M.MP.update_model_parameters(f=10.,
                             sigma_ground=[1e-4, 1e-1])

# set up the variational formulations
M.FE.build_var_form()  # the Tx information is passed automatically
                       # with the mesh parameters JSON file

# call solver and convert H-fields
M.solve_main_problem()

# create interpolation meshes
M.IB.create_slice_mesh('z', 2e3, 100, z=-0.1,  # 10cm below surf.
                       slice_name='surface')
M.IB.create_slice_mesh('z', 2e3, 100, z=50.,
                       slice_name='surface_50m_above')
slicE_0 = 'surface_slice_z'
slicE_50 = 'surface_50m_above_slice_z'

# interpolate fields on the observation slices
M.IB.interpolate([slicE_0, slicE_50])
