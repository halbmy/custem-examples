# -*- coding: utf-8 -*-
"""
Created on Tue Nov 12 08:49:16 2019

@author: Rochlitz.R
"""


import numpy as np
from custEM.core import MOD


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # #                        Notice !!!                         # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# If random crashes occur during the solution phase, run the three approaches
# one after each other (not in the same call) and everything works fine,
# we haven't found the reason for such PETSc/MUMPs crashes yet

# %% specify polynomial order

#p = 1  # run with 4 MPI procs for good performance
p = 2  # run with 12 MPI procs for good performance

# %%  model, mes, rx
mod = 'p' + str(p)
mesh = 'e1_layered'
rx_origs = np.array([[0., 0., -0.1],
                     [150., 100., -0.1]])

# # %% run computations, select approaches to use for calculation
# approaches = ['E_RA',  # rational arnoldi
#               'E_FT',  # fourier-transform based
#               'E_IE',  # implicit euler
#               ]

# for approach in approaches:

#     # initialize model amd parameters
#     M = MOD(mod, mesh, approach, p=p, overwrite_results=True,
#             overwrite_mesh=True, serial_ordering=True)

#     M.IB.create_path_mesh(rx_origs, path_name='e1_rec')
#     M.MP.update_model_parameters(sigma_ground=[1e-2, 1e-1, 2e-3])

#     M.FE.build_var_form(n_lin_steps=10,  # only relevant for IE approach
#                         n_log_steps=41,
#                         source_type='djdt',  # only relevant for IE approach
#                         log_t_min=-6,
#                         log_t_max=-2,
#                         interp_mesh='e1_rec_path',
#                         )

#     # solve FE problem and interpolate results
#     M.solve_main_problem(export_pvd=False)
#     #M.PP.FE.transform_to_time_domain('e1_rec_path', ['E_t', 'H_t'])

#     if 'FT' not in approach:  # IE and RA approach interpolation
#         M.IB.interpolate('e1_rec_path')

#     M.PP.merge_td_results('e1_rec_path')


# %% run further computations for B-field calculation

# run arnoldi with more times for time-integration

M = MOD(mod + '_j', mesh, 'E_RA', p=p, overwrite_results=True,
        overwrite_mesh=True, serial_ordering=True)

M.MP.update_model_parameters(sigma_ground=[1e-2, 1e-1, 2e-3])

M.FE.build_var_form(n_log_steps=71 + 140,
                    log_t_min=-8,
                    log_t_max=-1,
                    pole_shift=1.)

M.solve_main_problem(interp_mesh='e1_rec_path')  # only relevant for FT
M.IB.interpolate('e1_rec_path')
M.PP.merge_td_results('e1_rec_path')


# run implicit Euler vor B instead of dB/dt

# shut off
M = MOD(mod + '_j_off', mesh, 'E_IE', p=p, overwrite_results=True,
        overwrite_mesh=True, serial_ordering=True)

M.MP.update_model_parameters(sigma_ground=[1e-2, 1e-1, 2e-3])

M.FE.build_var_form(n_lin_steps=60,  # only relevant for IE approach
                    n_log_steps=41,
                    source_type='j',  # only relevant for IE approach
                    be_order=2,
                    log_t_min=-6,
                    log_t_max=-2)

M.solve_main_problem(interp_mesh='e1_rec_path')  # only relevant for FT
M.IB.interpolate('e1_rec_path')
M.PP.merge_td_results('e1_rec_path')

# shut on
M = MOD(mod + '_j_on', mesh, 'E_IE', p=p, overwrite_results=True,
        overwrite_mesh=True, serial_ordering=True)

M.MP.update_model_parameters(sigma_ground=[1e-2, 1e-1, 2e-3])

M.FE.build_var_form(n_lin_steps=60,  # only relevant for IE approach
                    n_log_steps=41,
                    source_type='j',  # only relevant for IE approach
                    shut_off=False,
                    be_order=2,
                    log_t_min=-6,
                    log_t_max=-2)

M.solve_main_problem(interp_mesh='e1_rec_path')  # only relevant for FT
M.IB.interpolate('e1_rec_path')
M.PP.merge_td_results('e1_rec_path')

