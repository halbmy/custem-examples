# -*- coding: utf-8 -*-
"""
Created on Wed Jun 20 15:05:51 2018

@author: Skibbe.N

"""
from pyhed.hed.reference import convertCSEM

freq = [1., 10., 100., 1000.]

for f in freq:
    convertCSEM('f_{:.0f}.csem'.format(f), 'line4freq.bms',
                out_name='dipole1d_freq_{:.0f}'.format(f))

# The End
