#!/usr/bin/env python
# coding: utf-8

# # MarlimR3D - Meshes and Fields
#
# **A big THANK YOU to [@Bane](https://github.com/banesullivan) for the help with PyVista to plot the tetrahedra mesh!**

# In[1]:


import discretize
import numpy as np
import xarray as xr
import pyvista as pv
import matplotlib.pyplot as plt
from scipy.spatial import ConvexHull
from matplotlib.colors import LogNorm


data = xr.load_dataset('data/marlim_survey.nc', engine='h5netcdf')
path = "./results/E_RA/e3_marlim_results/"
mesh = pv.read(path+"p2_Domains000000.pvtu")
res_h = pv.read(path+"p2_res_h000000.pvtu")


def create_cartesian_slice_for_mpl(mesh, normal='x', origin=None, scalars=None):
    """This only works if the slice is to be along a cartesian axes

    See https://github.com/pyvista/pyvista-support/issues/70

    normal is a str of the axes norma to slice (x, y, or z)
    origin is location of slice.
    scalars is name of array to use.
    """
    data = mesh.slice(normal=normal, origin=origin, generate_triangles=True)
    assert data.is_all_triangles()

    # Grab data for Matplotlib
    x = data.points
    tri = data.faces.reshape((-1, 4))[:, 1:]
    if scalars is None:
        u = data.active_scalars
    else:
        u = data[scalars]

    if normal == 'x':
        a, b = x[:,1], x[:,2] # YZ
    elif normal == 'y':
        a, b = x[:,0], x[:,2] # XZ
    elif normal == 'z':
        a, b = x[:,0], x[:,1] # XY
    else:
        raise ValueError("Normal must be 'x', 'y', or 'z' to align with cartesian axes.")

    return a, b, tri, u


# Broadside => +1km from source in y-direction.
# xtri, ytri, tri, _ = create_cartesian_slice_for_mpl(mesh, 'y', (0, +1000, 0))
xtri, ytri, tri, res = create_cartesian_slice_for_mpl(res_h, 'y', (0, +1000, 0))

xtri += 390275

def crinkle_slice(mesh, *args, **kwargs):
    """Returns crinkled and normal slice. Same arguments as slice filter"""
    mesh['cell_ids'] = np.arange(0, mesh.n_cells, dtype=int)
    slc = mesh.slice(normal='y', *args, **kwargs)
    cut = mesh.extract_cells(np.unique(slc['cell_ids']))
    return cut, slc

# Get crinkled slice
cnk, slc = crinkle_slice(mesh, generate_triangles=False)
# cnk2, slc2 = crinkle_slice(res_h, generate_triangles=False)

a = cnk.active_scalars

# Take a screenshot in PyVista
p = pv.Plotter(off_screen=True, notebook=False)
p.enable_parallel_projection()
p.add_mesh(cnk, scalars=a-1, clim=[0, 6], cmap='Dark2')
p.camera_position = 'xz'
img = p.screenshot(return_img=True, window_size=(5000, 5000))
p.close()

# p2 = pv.Plotter(off_screen=True, notebook=False)
# p2.enable_parallel_projection()
# p2.add_mesh(cnk2, color='blue')
# p2.camera_position = 'xz'
# img2 = p2.screenshot(return_img=True, transparent_background=True, window_size=(5000, 5000))
# p2.close()


# print(np.argwhere(img[:, :, 3] > 0.0).shape)
# print(img[:, :, 3].shape)

# points = np.argwhere(img[:, :, 3] > 0.0)
# hull = ConvexHull(points)

# boundary = points[hull.vertices]
# # Close the loop
# boundary = np.append(boundary, boundary[0][None], axis=0)
# ix = boundary[:,0]
# iy = boundary[:,1]

ix = [792, 4207, 4207, 792, 792]
iy = [792, 4207, 4207, 792, 792]


pad = 10
texture = img[np.min(ix)+pad:np.max(ix)-pad, np.min(iy)+pad:np.max(iy)-pad, :3] # @ [0.1, 0.1, 0.1] # [0.299, 0.587, 0.114]
#texture2 = img2[np.min(ix)+pad:np.max(ix)-pad, np.min(iy)+pad:np.max(iy)-pad, :3]  @ [0.299, 0.587, 0.114]

# print(texture.min(), texture.max())
# bla

bounds = np.r_[mesh.bounds[:2], mesh.bounds[4:6]]
bounds[:2] += 390275


# Define some appearances here, so we can easily change them for all
lc = '0.4'
lw = 0.5

vnorms = {'vmin': 1e-1, 'vmax': 1e4}

# Min/max of all meshes.
xmin = 390275 - 25080.
xmax = 390275 + 25080.
zmin = -25080.
zmax = 0.


# Initiate figure (axes shared)
fig, axs = plt.subplots(1, 2, figsize=(12, 6), sharex=True, sharey=True,
                        subplot_kw={'aspect': 1., 'adjustable': 'box'})
(ax1, ax2) = axs
# plot mesh
vrange = (texture.max()-texture.min())/3

ax1.imshow(texture, aspect=1., interpolation='bicubic',
           vmin=texture.min(), vmax=texture.max(), extent=bounds)

# plot resitivities
cb = ax2.tripcolor(xtri, ytri, tri, res, shading='flat', antialiased=True, linewidth=0.01,
                   edgecolors='0.9', norm=LogNorm(**vnorms))

# Plot survey over meshes.
ax1.plot(data.src_x[::2], data.data_bs.attrs['src_z'],
         'k.', markersize='1')


ax1.set_xlim([data.rec_x - 25080., data.rec_x + 25080.])
ax1.set_ylim([-25080., 0.])


# Titles and labels.
ax1.set_title('mesh')
ax2.set_title('horizontal resitivities')
ax1.set_xlabel('position on broadside line (m)')
ax2.set_xlabel('position on broadside line (m)')
ax1.set_ylabel('depth (m)')
ax2.set_ylabel('depth (m)')

ax1.yaxis.set_ticks_position('both')
ax2.yaxis.set_ticks_position('both')


# make tight, leave space for colorbar.
plt.tight_layout(rect=[0, 0, 0.95, 1])

# colorbar.
c = fig.colorbar(cb, ax=axs, fraction=0.011)
c.ax.set_title(r'$\Omega$m')

# save and show.
plt.savefig('plots/marlim_mesh.pdf', bbox_inches='tight')
plt.show()
