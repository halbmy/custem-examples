#!/usr/bin/env python
# coding: utf-8

# # MarlimR3D - Comparison

# In[1]:


import numpy as np
import xarray as xr
import matplotlib.pyplot as plt
# import loadmarlim

# data = xr.load_dataset('marlim_survey.nc', engine='h5netcdf')
# loadmarlim.create_survey(store_data=True)

data = xr.load_dataset('data/marlim_survey.nc', engine='h5netcdf')
#data = xr.load_dataset('ref/marlim_emg3d.nc', engine='h5netcdf')

# Receiver positions (reciprocity)
offs = data.src_x[::2] - data.rec_x
offs += 390275

print(offs[0])

# Line styles
ls = ['c', 'y', 'm', 'g', 'r', 'b']
colors = plt.rcParams["axes.prop_cycle"].by_key()["color"]
colors.pop(5)



#ls = ['co', 'g', 'm*', 'gd', 'y', 'y^']

def extract_line(d, n, b=None):
    data = getattr(d, n)
    # data = np.abs(data.data[::2, :, :] + 1j*data.data[1::2, :, :])
    data = data.data[::2, :, :] + 1j*data.data[1::2, :, :]

    if b is not None:
        comp = getattr(b, n)
        comp = np.abs(comp.data[::2, :, :] + 1j*comp.data[1::2, :, :])
        comp = comp.data[::2, :, :] + 1j*comp.data[1::2, :, :]
        data -= comp

    return data


il = 'data/inline.npy'
bs = 'data/broadside.npy'

# In[6]:


# fig, axs = plt.subplots(3, 2, figsize=(9, 10), sharex=True, sharey=True)

# # Loop over Inline/Broadside
# for iii, datname in enumerate(['data_il', 'data_bs']):

#     # Get absolute values of this line
#     tdat = extract_line(data, datname)
#     if iii == 0:
#         export = np.load(il)
#     else:
#         export = np.load(bs)

#     # Loop over components Ex, Ey, Ez
#     for ii, comp in enumerate(data.components.values[:3]):

#         plt.sca(axs[ii, iii])
#         plt.title(f"{['Inline', 'Broadside'][iii]} :: {comp}")

#         # Loop over frequencies
#         for i, freq in enumerate(data.freqs.values):

#             if iii == 0:
#                 np.save('data/' + str(freq) + '_il.npy', tdat[:, i, ii])
#                 ref = np.load('./data/f_' + str(freq) + '_interpolated/'
#                               'E_t_on_inline_path_line_x.npy')[::-1] / 10.
#             elif iii == 1:
#                 np.save('data/' + str(freq) + '_bs.npy', tdat[:, i, ii])
#                 ref = np.load('./data/f_' + str(freq) + '_interpolated/'
#                               'E_t_on_broadside_path_line_x.npy')[::-1] / 10.

#             # Plot this component/frequency
#             plt.plot(offs[:102], np.abs(tdat[:102, i, ii]), ls[i][0]+'-', lw=1, label=f"{freq:4.3f}")
#             plt.plot(offs[102:], np.abs(tdat[102:, i, ii]), ls[i][0]+'-', lw=1)
#             # plt.plot(offs[:102], np.abs(ref[:102, ii]), ls[i][0]+'-', lw=1, label=f"{freq:4.3f}")
#             # plt.plot(offs[102:], np.abs(ref[102:, ii]), ls[i][0]+'-', lw=1)
#             plt.plot(offs[:102], np.abs(export[ii, :102, i]), 'k:', lw=1, label="tdcustem")
#             plt.plot(offs[102:], np.abs(export[ii, 102:, i]), 'k:', lw=1)

#         plt.axhline(1e-15)
#         # plt.legend(title='f (Hz)', loc='lower center', ncol=3)
#         plt.grid('on')
#         plt.yscale('log')
#         plt.ylim([1e-18, 1e-10])

#         if ii == 2:
#             plt.xlabel('Offset (m)')
#         if iii == 0:
#             plt.ylabel('E (V/Am$^2$)')
#         else:
#             axs[ii, iii].yaxis.set_ticks_position('right')
#             axs[ii, iii].yaxis.set_label_position('right')

# plt.tight_layout()
# plt.savefig('MARLIM_R3D_Arnoldi_TD_comparison.png')

fig, axs = plt.subplots(2, 3, figsize=(12, 7), sharex=True, sharey=False)

for iii, datname in enumerate(['data_bs', 'data_bs']):

    # Get absolute values of this line
    tdat = extract_line(data, datname)
    if iii == 0:
        export = np.load(bs)
    else:
        export = np.load(bs)

    # Loop over components Ex, Ey, Ez
    for ii, comp in enumerate(data.components.values[:3]):

        plt.sca(axs[iii, ii])
        if iii == 0:
            plt.title(f"{['Broadside'][iii]} -- {comp}")

        # Loop over frequencies
        for i, freq in enumerate(data.freqs.values):

            if iii == 0:
                ref = np.load('./data/f_' + str(freq) + '_interpolated/'
                              'E_t_on_inline_path_line_x.npy')[::-1] / 10.
            elif iii == 1:
                ref = np.load('./data/f_' + str(freq) + '_interpolated/'
                              'E_t_on_broadside_path_line_x.npy')[::-1] / 10.

            misfit1 = 200 * (np.abs(export[ii, :, i]) -
                             np.abs(tdat[:, i, ii])) /\
                             (np.abs(export[ii, :, i]) +
                             np.abs(tdat[:, i, ii]))
            if iii == 0:
                if i == 0:
                    plt.axhline(1e-15, color='k', label='1e-15 V/m')
                plt.plot(offs[:102], np.abs(tdat[:102, i, ii]), '-', color=colors[i], lw=1., label=f"C&M {freq:4.3f} Hz")
                plt.plot(offs[102:], np.abs(tdat[102:, i, ii]), '-', color=colors[i], lw=1.)
                plt.plot(offs[:102], np.abs(export[ii, :102, i]), 'k:', lw=1)
                if i == 5:
                    plt.plot(offs[102:], np.abs(export[ii, 102:, i]), 'k:', lw=1., label='custEM RA')
                else:
                    plt.plot(offs[102:], np.abs(export[ii, 102:, i]), 'k:', lw=1.)

                plt.grid('on')
                plt.yscale('log')
                plt.ylim([1e-18, 1e-10])
            else:
                plt.plot(offs[:102], np.abs(misfit1[:102]), '-', color=colors[i], lw=0.5,
                          label='misfit @ ' + str(freq) + ' Hz')
                plt.plot(offs[102:], np.abs(misfit1[102:]), '-', color=colors[i], lw=0.5)
                plt.grid('on')
                plt.yscale('log')
                plt.ylim([1e-2, 1e2])

        if iii == 0:
            plt.ylabel('||E|| (V/m)')
        else:
            plt.ylabel('misfit (%)')
            plt.xlabel('position on broadside line (m)')

        if ii == 1:
            axs[iii, ii].set_yticklabels([])
            axs[iii, ii].minorticks_off()
            axs[iii, ii].set_ylabel('')
        if ii == 2:
            axs[iii, ii].yaxis.set_ticks_position('right')
            axs[iii, ii].yaxis.set_label_position('right')

    if iii == 0:
        plt.legend(fontsize=8, loc='lower center')
    else:
        plt.legend(fontsize=8, ncol=2)
plt.tight_layout()
plt.savefig('plots/RA_CM_misfits.pdf')
plt.show()

