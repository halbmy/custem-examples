# custEM: Modeling and inversion examples



## Modeling

- Frequency-domain (CSEM), time-domain (TEM), and magentotelluric (MT) forward modeling exmaples
- Examples deal with different environments, anisotropy, induced-polarization, and others.
- We would be glad to add further modeling application with custEM by others

## Inversion

- Inversion examples, several succesful test cases should be added here in future 

## SupplementsGJI

- Supplementary material for GJI publication including the corresponding inversion examples 
