# Synthtic data - Example 1 in GJI publication

- information and plots for our results are available in the results_2022_10 directory
- our results can be reproduced by running the following files

## 01 generate mesh for synthetic model and calculate synthetic data

## 02 generate inversion mesh with proper discretizations for either p1 or p2 and run inversion

## 03 generate data and misfits plots

## 04 view inversion results on slices