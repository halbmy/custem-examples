import matplotlib
matplotlib.use('pdf')

import numpy as np
from saem import CSEMSurvey

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # #  prepare processed data for inversion   # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


# %% Load Tx position and emdata files

# load Tx pos
tx1 = np.loadtxt("data/tx1.pos")
tx2 = np.loadtxt("data/tx2.pos")
tx11 = np.loadtxt("data/tx11.pos")
tx12 = np.loadtxt("data/tx12.pos")
tx13 = np.loadtxt("data/tx13.pos")

file1 = "data/Schleiz_Tx01_Tx02_L01_L13_BxByBz_47deg_estmationerrors.emdata"
file2 = "data/Schleiz_Tx11_Tx12_L01_L13_BxByBz_47deg_estmationerrors.emdata"
file3 = "data/Schleiz_Tx13_L01_L14_BxByBz_47deg_estmationerrors.emdata"

data1 = CSEMSurvey(file1, flipxy=True, txs=[tx1, tx2])
data2 = CSEMSurvey(file2, flipxy=True, txs=[tx11, tx12])
data3 = CSEMSurvey(file3, flipxy=True, txs=[tx13])

# %% define parameters

# local coordinate system transformation
origin = [701449.9, 5605079.4, 0.]
angle = 47.
line_spacing_vector = np.linspace(-1500., 1600., 13)

# use (rotated) x and z components only, and sort out data with less than 500m
# distance to Tx (or 320m for Tx13)
cmp = [1, 0, 1]
min_tx_dist = 500.
min_tx_dist_13 = 320.

reduce = 2           # use only every second data point
error = 0.05         # relative error of 5 %
noise_B = 2e-3       # noise level

# %% Adjust patches Tx1 and Tx2

for pi, txid in enumerate(['1', '2']):
    # define lines
    data1.patches[pi].detectLines(line_spacing_vector, axis='y', show=True)
    # reduce data density
    data1.patches[pi].filter(every=reduce)
    # select frequency range to use
    data1.patches[pi].filter(fmin=7, fmax=1100)
    # sort out single frequencies which have no good data quality
    data1.patches[pi].filter(f=10)
    data1.patches[pi].filter(f=14)
    data1.patches[pi].filter(f=48)
    # sort out selected data which are not reliable
    if pi == 1:
        data1.patches[pi].DATA[:, :5, :] = np.nan + 1j * np.nan
    # sort out data in vicinity of Tx
    data1.patches[pi].filter(minTxDist=min_tx_dist)
    # define error model
    data1.patches[pi].estimateError(absError=noise_B, relError=error)
    # sort out data with very weak amplitudes
    data1.patches[pi].deactivateNoisyData(aErr=0.0001, rErr=0.5)
    # set parameters
    data1.patches[pi].basename='Tx' + txid + 'BxBz_every' + str(reduce)
    data1.patches[pi].origin = origin
    data1.patches[pi].angle = angle
    data1.patches[pi].cmp = cmp
    # generate data PDF and save data of individual patches
    data1.patches[pi].generateDataPDF()
    data1.patches[pi].saveData(fname='data/' + data1.patches[pi].basename)

# %% Adjust patches Tx11 and Tx12

for pi, txid in enumerate(['11', '12']):
    data2.patches[pi].detectLines(line_spacing_vector, axis='y', show=True)
    data2.patches[pi].filter(every=reduce)
    data2.patches[pi].filter(fmin=7, fmax=1100)
    data2.patches[pi].filter(f=10)
    data2.patches[pi].filter(f=14)
    data2.patches[pi].filter(f=48)
    data2.patches[pi].filter(f=72)
    data2.patches[pi].filter(minTxDist=min_tx_dist)
    if pi == 0:
        data2.patches[pi].DATA[:, :8, :] = np.nan + 1j * np.nan
    else:
        data2.patches[pi].DATA[:, :3, data2.patches[pi].line==9] =\
            np.nan + 1j * np.nan
    data2.patches[pi].estimateError(absError=noise_B, relError=error)
    data2.patches[pi].deactivateNoisyData(aErr=0.0001, rErr=0.5)
    data2.patches[pi].basename='Tx' + txid + 'BxBz_every' + str(reduce)
    data2.patches[pi].origin = origin
    data2.patches[pi].angle = angle
    data2.patches[pi].cmp = cmp
    data2.patches[pi].generateDataPDF()
    data2.patches[pi].saveData(fname='data/' + data2.patches[pi].basename)

# %% Adjust patche Tx13

for pi, txid in enumerate(['13']):
    data3.patches[pi].filter(nInd=np.nonzero(data3.patches[pi].ry>-1800.)[0])
    data3.patches[pi].filter(nInd=np.nonzero(np.logical_or(
        data3.patches[pi].rx<9800., data3.patches[pi].line<10))[0])
    data3.patches[pi].detectLines(line_spacing_vector, axis='y', show=True)
    data3.patches[pi].DATA[2, 0, :] = np.nan + 1j * np.nan
    data3.patches[pi].filter(every=reduce)
    data3.patches[pi].filter(fmin=7, fmax=1100)
    # manipulate frequencies to fit with other data sets
    # (avoid conflicts due to minor differences of values which were caused by
    # the individual processing of the patches)
    data3.patches[pi].f = np.array([7.46269,  22.3881,   31.25,
                                    66.8866,   90.5097,  124.565,
                                    176.263, 256.,    362.039,
                                    512.,   724.077,  1024.])
    data3.patches[pi].filter(minTxDist=min_tx_dist_13)
    data3.patches[pi].estimateError(absError=noise_B, relError=error)
    data3.patches[pi].deactivateNoisyData(aErr=0.0001, rErr=0.5)
    data3.patches[pi].basename='Tx' + txid + 'BxBz_every' + str(reduce)
    data3.patches[pi].origin = origin
    data3.patches[pi].angle = angle
    data3.patches[pi].cmp = cmp
    data3.patches[pi].generateDataPDF()
    data3.patches[pi].saveData(fname='data/' + data3.patches[pi].basename)

# %% merge single patches for combined inversion and save as new data file
combined = CSEMSurvey()
combined.addPatch('data/Tx1BxBz_every' + str(reduce) + '.npz')
combined.addPatch('data/Tx2BxBz_every' + str(reduce) + '.npz')
combined.addPatch('data/Tx11BxBz_every' + str(reduce) + '.npz')
combined.addPatch('data/Tx12BxBz_every' + str(reduce) + '.npz')
combined.addPatch('data/Tx13BxBz_every' + str(reduce) + '.npz')

combined.basename='data/Combined_every' + str(reduce)
combined.saveData()