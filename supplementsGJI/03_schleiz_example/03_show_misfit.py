# -*- coding: utf-8 -*-
"""
Created on Tue Nov  7 12:21:17 2017

@author: Rochlitz.R
"""


import matplotlib
# matplotlib.use('pdf')
from saem import CSEMSurvey
from matplotlib import cm, rcParams
import matplotlib.pyplot as plt


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # # plot data and final response misfit # # # # # # # # # # #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


# %% import results and use plot utilities from SAEM class

# specify p
p = 1

# import results
data = CSEMSurvey('data/Combined_every2BxBz.npz')
data.loadResults(dirname='inv_results/p' + str(p) + '_combined_inv_p' + str(p))

# patches[0] ... Bx' & Bz for Tx 1
# patches[1] ... Bx' & Bz for Tx 2
# patches[2] ... Bx' & Bz for Tx 11
# patches[3] ... Bx' & Bz for Tx 12
# patches[4] ... Bx' & Bz for Tx 13

# %% generate pdf files

for pi, patch in enumerate(data.patches):
    patch.generateDataPDF(pdffile='p' + str(p) + '_misfit_patch_' +
                          str(pi) + '.pdf', mode='linefreqwise')